package timer.timer;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class TimerController {

    @CrossOrigin
    @GetMapping("/time")
    public ResponseEntity<Map<String, Object>> getTime() {
        LocalDateTime currentTime = LocalDateTime.now();

        Map<String, Object> response = new HashMap<>();
        response.put("time", currentTime.toString());
        return ResponseEntity.ok().body(response);
    }
}
