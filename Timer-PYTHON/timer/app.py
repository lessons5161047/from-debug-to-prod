from flask import Flask
from datetime import datetime
from flask_cors import CORS 

app = Flask(__name__)
CORS(app)
@app.route('/api/time')
def index():
    current_date_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    return {'time':current_date_time }
  
#if __name__ == "__main__":
#     app.run(debug=False, host='0.0.0.0', port=5000)
