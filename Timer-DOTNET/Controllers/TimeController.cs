﻿using Microsoft.AspNetCore.Mvc;

namespace Timer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TimeController : ControllerBase
    {

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(new { time = System.DateTime.Now });
        }
    }
}
