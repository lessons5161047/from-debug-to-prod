import Clock from "react-clock";

export default function ClockWrapper({ time, name }) {
  return (
    <div className="clock-wrapper">
      <Clock value={time} />
      <p>{name}</p>
    </div>
  );
}
