import axios from "axios";
import { useQuery } from "@tanstack/react-query";

const GetDotNetTime = () => {
  return axios.get(import.meta.env.VITE_DOTNET);
};

export const DotNetTime = () => {
  return useQuery({ queryKey: ["DotNetTime"], queryFn: GetDotNetTime, refetchInterval: 1 * 1000 });
};

const GetJSTime = () => {
  return axios.get(import.meta.env.VITE_JS);
};

export const JSTime = () => {
  return useQuery({ queryKey: ["JSTime"], queryFn: GetJSTime, refetchInterval: 1 * 1000 });
};

const GetJavaTime = () => {
  return axios.get(import.meta.env.VITE_JAVA);
};

export const JavaTime = () => {
  return useQuery({ queryKey: ["JavaTime"], queryFn: GetJavaTime, refetchInterval: 1 * 1000 });
};

const GetPythonTime = () => {
  return axios.get(import.meta.env.VITE_PYTHON);
};

export const PythonTime = () => {
  return useQuery({ queryKey: ["PythonTime"], queryFn: GetPythonTime, refetchInterval: 1 * 1000 });
};
