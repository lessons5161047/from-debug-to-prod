import "./App.css";
import ClockWrapper from "./ClockWrapper";
import { JSTime, DotNetTime, JavaTime, PythonTime } from "./Data/GetTime";

function App() {
  const { isLoading: JSTimeIsLoading, data: JSTimeData, isError: JSTimeIsError } = JSTime();
  const { isLoading: DotNetTimeIsLoading, data: DotNetTimeData, isError: DotNetTimeIsError } = DotNetTime();
  const { isLoading: JavaTimeIsLoading, data: JavaTimeData, isError: JAvaTimeIsError } = JavaTime();
  const { isLoading: PythonTimeIsLoading, data: PythonTimeData, isError: PythonTimeIsError } = PythonTime();
  return (
    <>
      <h1>Current time from different backends</h1>
      <div className="flex-container">
        {JSTimeIsLoading || JSTimeIsError ? <p>Loading...</p> : <ClockWrapper time={JSTimeData.data.time} name={"JSTime"} />}
        {DotNetTimeIsLoading || DotNetTimeIsError ? <p>Loading...</p> : <ClockWrapper time={DotNetTimeData.data.time} name={"DotNetTime"} />}
        {JavaTimeIsLoading || JAvaTimeIsError ? <p>Loading...</p> : <ClockWrapper time={JavaTimeData.data.time} name={"JavaTime"} />}
        {PythonTimeIsLoading || PythonTimeIsError ? <p>Loading...</p> : <ClockWrapper time={PythonTimeData.data.time} name={"PythonTime"} />}
      </div>
    </>
  );
}

export default App;
