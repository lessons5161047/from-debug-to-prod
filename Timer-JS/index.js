const express = require("express");
const app = express();
const cors = require("cors");
const port = 3000;

const corsConfig = {
  origin: true,
  credentials: true,
};

app.use(cors(corsConfig));
app.options("*", cors(corsConfig));
app.use(express.json());

app.get("/api/time", function (req, res) {
  const time = new Date();
  res.json({ time });
});

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
